## Oatmeal Raisin Cookies

Oatmeal raisin cookies are chewy and sweet.  These cookies are one of my favorite cookie recipes.

## Description

The sweetness from the raisins and brown sugar, the hint of cinnamon, and the texture of the oats makes this a great cookie recipe.

I've read a lot of recipes for oatmeal raisin cookies and I think the description of "soft and chewy" is perfect for these cookies.

This recipe makes about 3 dozen cookies.

## Image

<img src="{{ url_for('static', filename='img/oatmeal_raisin_cookies.jpg') }}" alt="Oatmeal Raisin Cookies" />

## Tips

Don't over-mix the batter once the flour has been incorporated, as this can make the dough tough. 

An ice cream scoop is good for getting out even-sized balls of dough onto the cookie sheet. 

If the butter isn't at room temperature, put the butter in the microwave for about 35 seconds to warm it up.

## Ingredients

* 1 Cup of Dairy-Free Butter softened to room temperature (recommendation: [Earth Balance - Soy Free Butter Spread](https://www.earthbalancenatural.com/spreads/soy-free-buttery-spread)
* 3/4 Cup of Brown Sugar (Light)
* 1/2 Cup of Granulated Sugar
* 2 Eggs
* 1 Teaspoon of Vanilla Extract
* 1.5 Cups of All-Purpose Flour
* 1 Teaspoon of Baking Soda
* 1 Teaspoon of Cinnamon
* 1/2 Teaspoon of Salt
* 3 Cups of Rolled Oats
* 1.5 Cups of Raisins

## Steps

1. Preheat oven to 350°F.
2. In a stand mixer with the paddle attachment, cream the dairy-free butter and both sugars for 3 minutes on medium speed.
3. Add eggs and vanilla; mix for 1 additional minute.
4. Combine flour, baking soda, cinnamon, and salt in a medium-bowl; add this dry mixture and mix until combined with the wet ingredients.
5. Add oats and raisins; mix until combined.
6. Drop rounds of dough using tablespoons onto an un-greased cookie sheet.
7. Bake cookies for 11-12 minutes or until light golden brown on the bottom.
